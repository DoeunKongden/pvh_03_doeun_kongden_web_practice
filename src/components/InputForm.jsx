import React, { Component } from "react";

export default class InputForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showName: false,
            showAge: false,
            info: [
                { name: "momo", age: 17 }
            ],
            newName: "null",
            newAge: "null",
            nameError: "",
            ageError: "",
        }
    }

    handleChangeName = (values) => {
        this.setState({
            newName: values.target.value
        });
    }
    handleChangeAge = (values) => {
        this.setState({
            newAge: values.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const newInfo = {
            name: this.state.newName,
            age: this.state.newAge
        }

        if (this.state.newName === "null") {
            this.setState({
                showName: true,
                nameError: "Please Enter Name"
            })
        }
        if(this.state.newAge === "null" | !/[0-9]/.test(this,this.state.newAge)){
            this.setState({
                showAge: true,
                ageError: "Please Enter Age"
            })
            console.log(this.state.showAge)
        }else{
            this.setState({
                info: [...this.state.info, newInfo], newName: "null", newAge: "null",
                showAge: false,
                showName: false
            }, () => console.log("New Array :", this.state.info))
        }
        console.log(this.state.show);
    }

    render() {
        return (
            <div className="max-w-[900px] m-auto py-10">
                <h1 className="text-3xl font-bold text-center">Validate Form HomeWork</h1>
                <form className="mt-10">
                    <div className="mb-6 w-full">
                        <label for="username-success" className="block mb-2 text-sm font-medium text-green-700">Name</label>
                        <input type="text" id="username-success" onChange={this.handleChangeName} class=" border border-green-500 text-green-900 placeholder-green-700 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5" />
                        {this.state.showName ? <p class="mt-2 text-sm text-red-600 "><span class="font-medium"></span>{this.state.showName ? this.state.nameError : ""}</p> :null }
                    </div>
                    <div>
                        <label for="username-error" className="block mb-2 text-sm font-medium text-green-700">Age</label>
                        <input type="text" id="username-error" onChange={this.handleChangeAge} className="border border-green-500 text-sm rounded-lg focus:ring-green-500 focus:border-green-500 block w-full p-2.5" />
                        {this.state.showAge ? <p class="mt-2 text-sm text-red-600"><span class="font-medium"></span>{this.state.showAge ? this.state.ageError : ""}</p> : null}
                    </div>
                    <button type="button" onClick={this.handleSubmit} className="text-blue-700 w-80 m-auto flex justify-center my-10 hover:text-white border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">Submit</button>
                </form>
            </div>
        )
    }
}